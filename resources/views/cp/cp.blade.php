@extends('statamic::layout')

@section('title', 'Backup')

@section('content')
    <header class="mb-3">

        @include('statamic::partials.breadcrumb', [
            'url' => cp_route('utilities.index'),
            'title' => __('Utilities')
        ])
        <div class="flex items-center justify-between">
            <h1>Backup</h1>
        </div>
    </header>
    <section id="app">
        <create-backup
                cp-route="{{ cp_route('backup.create') }}"
                downloads-route="{{cp_route('backups')}}"
                delete-route="{{cp_route('backup.delete')}}"
                restore-route="{{cp_route('backup.restore')}}"
        >

        </create-backup>

    </section>

@endsection
