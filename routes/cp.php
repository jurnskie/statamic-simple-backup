<?php

use Illuminate\Support\Facades\Route;
use Acme\Backup\Http\Controllers\BackupController;


Route::post('backup/create', ['\\'. BackupController::class, 'create'])->name('backup.create');
Route::get('backup/', ['\\'. BackupController::class, 'show'])->name('backup.show');
Route::get('backups', ['\\'. BackupController::class, 'index'])->name('backups');
Route::post('backup/delete-backup', ['\\'. BackupController::class, 'delete'])->name('backup.delete');
Route::post('backup/restore-backup', ['\\'. BackupController::class, 'restore'])->name('backup.restore');