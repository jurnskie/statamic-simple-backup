<?php

namespace Acme\Backup\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\Yaml\Yaml;

use Illuminate\Support\Carbon;


use Illuminate\Support\Facades\File;
use ZipArchive;

class BackupController extends BaseController{


    /**
     * BackupController constructor.
     */
    public function __construct()
    {
        $this->backup_path =  public_path() . '/backups/';
        $this->settings_file = '/app/backups/backups.yaml';
    }


    /**
     * @return mixed
     */
    public function __invoke()
    {

        $this->sync_backups();

        if(file_exists(storage_path() . $this->settings_file)){

            $available_downloads = Yaml::parseFile(storage_path() . $this->settings_file);

        }else{
            $available_downloads = [];
        }


        return view('backup::cp.cp', [
            'downloads' => $available_downloads    
        ]);
    }

    /**
     * @return mixed
     */
    public function show(){



        $available_downloads = Yaml::parseFile(storage_path() . $this->settings_file);

        return view('backup::cp.cp', [
            'downloads' => $available_downloads
        ]);
    }

    public function sync_backups(){


        //empty yaml file
        $yamlFile = fopen(storage_path() . $this->settings_file,'r+');
        if($yamlFile !== false){
            ftruncate($yamlFile, 0);
            fclose($yamlFile);
        }


        $backups = scandir( $this->backup_path );
        $files = array_diff($backups, array('.', '..'));
        foreach($files as $backup){
            $explodedFileParts = explode('-', $backup);
            $currentDateTimeStamp = $explodedFileParts[0].'-'.$explodedFileParts[1].'-'.$explodedFileParts[2].'-'.$explodedFileParts[3];

            // check if yaml file exists, if it does append new row
           $this->writeYamlFile($currentDateTimeStamp, $backup);

        }



    }

    public function index(){

        $available_downloads = Yaml::parseFile(storage_path() . $this->settings_file);
        return $available_downloads;

    }

    /**
     * @return mixed
     */
    public function create(){


        $currentDateTimeStamp = Carbon::now()->toDateTimeString();


        // 2. Initialise zip file
        $zip_file = str_slug($currentDateTimeStamp) . '-' . 'statamic-backup' . '.zip';


        //2b. write zip file
        $this->writeZipFile($zip_file);

    
        //2c write yaml file
        $this->writeYamlFile($currentDateTimeStamp, $zip_file);

        
        return response()->json([
            'status' => 'success',
            'msg' => 'Created backup'
        ],201);



    }


    /**
     * @param $zip_file
     * @return mixed
     */
    public function writeZipFile($zip_file){

        $zip = new \ZipArchive();
        $zip->open($this->backup_path . $zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $path = base_path();
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));

        foreach($files as $name => $file){
            if (file_exists($file) && is_file($file)){
                // We're skipping all subfolders
                $filePath     = $file->getRealPath();

                // extracting filename with substr/strlen
                $relativePath = substr($filePath, strlen($path) + 1);

                $zip->addFile($filePath, $relativePath);
            }
       
            
        }

        $zip->close();

        $p = $this->backup_path . $zip_file;

        return response()->download($p, $zip_file);


    }


    public function writeYamlFile($currentDateTimeStamp,$zip_file){

        //if zip_file exists

        // if(file_exists( $this->backup_path . $zip_file )){

            $yamlRow = [
                'backup-' .str_slug($currentDateTimeStamp) => [
                    'created' => str_slug($currentDateTimeStamp),
                    'file_name' => $zip_file,
                    'download_link' => $this->backup_path . $zip_file
                ]
            ];

    
            // check if yaml file exists, if it does append new row 
            if(file_exists(storage_path() . $this->settings_file)){
    
                $settingsArray = Yaml::parseFile(storage_path() . $this->settings_file);

                if($settingsArray && count($settingsArray)){
                    $yamlRow = array_merge($yamlRow, $settingsArray);
                }
                
               
            }
    
    
            $yaml = Yaml::dump($yamlRow);

    
            return file_put_contents(storage_path() . $this->settings_file, $yaml);
        // }


     
    }

    /**
     *
     */
    public function delete(){

//        Storage::delete($this->backup_path . request()->fileName);
        File::delete($this->backup_path . request()->fileName);

        return $this->sync_backups();
    }

    public function restore(){

        // get filename out of request
        // unzip zip file over files
        $zip = new ZipArchive();
        if( $zip->open($this->backup_path . request()->fileName) === true){
            $zip->extractTo(base_path());
            $zip->close();
        }


        return response()->json([
            'status' => 'success',
            'msg' => 'Restored backup'
        ],201);


    }
    
}