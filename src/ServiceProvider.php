<?php

namespace Acme\Backup;

use Statamic\Providers\AddonServiceProvider;
use Acme\Backup\Http\Controllers\BackupController;
use League\Flysystem\File;
use Statamic\Facades\Utility;
use Statamic\Statamic;
use Illuminate\Support\Facades\Storage;



class ServiceProvider extends AddonServiceProvider
{
    protected $scripts = [
        __DIR__.'/../resources/dist/js/cp.js',
    ];

    protected $stylesheets = [
        __DIR__.'/../resources/dist/css/tailwind.css',

    ];

    protected $publishables = [
        __DIR__.'/../resources/images' => 'images',
    ];
    protected $routes = [
        'cp' => __DIR__.'/../routes/cp.php'
    ];

 

    // public function __construct()
    // {
    //     $this->backup_path =  public_path() . '/backups/';
    //     $this->settings_file = '/app/backups/backups.yaml';
    // }

    public function boot()
    {
        parent::boot();

   
        $utility = Utility::make('backup')
            ->action(BackupController::class)
            ->icon('hammer-wrench')
            ->description('Manage site backups with ease.');


        $utility->register();

        
        Statamic::afterInstalled(function ($command) {
        
            if(!file_exists(public_path() . '/backups/')){
            
                mkdir(public_path() . '/backups/',0777);
                
            }

            if(!Storage::disk('local')->exists('app/backups')){

                Storage::disk('local')->makeDirectory('app/backups');
                Storage::disk('local')->put('/app/backups/backups.yaml', '');

            }

        });
        

 
    }

}